#### 编程题

1、概述脚手架实现的过程，并使用 NodeJS 完成一个自定义的小型脚手架工具

实现过程：

    1、首先要明确项目需求

    2、找到合适的Generator

    3、全局安装Generator

    4、通过yo运行 Generator

    5、通过命令行交互填写选项

    6、生成所需要的项目结构


npm包已发布

npm包名:generator-handsome-vue

npm地址：https://www.npmjs.com/package/generator-handsome-vue

#### 全局安装

yarn add generator-handsome-vue --global 

#### 在文件路径中使用 

yo handsome-vue  

#### 安装依赖 

cnpm i  / yarn 

#### 项目运行 

npm run serve 

浏览器打开 http://localhost:8080/#/