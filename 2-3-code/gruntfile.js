const sass = require('sass');

const loadGruntTasks = require('load-grunt-tasks');

module.exports = grunt => {
    grunt.initConfig({
        //sass用于编译scss
        sass: {
            options: {
                implementation: sass,
                sourceMap: true
            },
            //项目文件编译 key为需要输出到的项目路径,value为项目文件的根路径
            main: {
                files: {
                    "dist/assets/styles/main.css": "src/assets/styles/main.scss"
                }
            }
        },
        //babel任务用于编译ES6+语法
        babel: {
            options: {
                presets: ['@babel/preset-env'],
                sourceMap: true

            },
            //项目文件编译 key为需要输出到的项目路径,value为项目文件的根路径
            main: {
                files: {
                    "dist/assets/scripts/app.js": "src/assets/scripts/*.js"
                }
            }
        },
        //监视文件改变，以及要运行的 任务
        watch: {
            js: {
                files: ['src/assets/scripts/*.js'],
                tasks: ['babel']
            },
            css: {
                files: ['src/assets/styles/*.scss'],
                tasks: ['sass']
            }
        }
    });

    //启用插件加载
    loadGruntTasks(grunt);

    //注册默认任务,sass => babel => watch
    grunt.registerTask('default', ['sass', 'babel', 'watch'])
}