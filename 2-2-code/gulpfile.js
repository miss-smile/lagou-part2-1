//引用内置的api
const { src, dest, parallel, series, watch } = require('gulp')

//用于转换ES6+ 语法
const babel = require('gulp-babel');

//用于删除文件插件
const del = require('del')

//浏览器自动刷新插件
const browserSync = require('browser-sync')

//自动加载gulp插件
const loadPlugins = require('gulp-load-plugins')

//语义化
const plugins = loadPlugins()

const bs = browserSync.create()

//mock数据
const data = {
    menus: [{
            name: 'Home',
            icon: 'aperture',
            link: 'index.html'
        },
        {
            name: 'Features',
            link: 'features.html'
        },
        {
            name: 'About',
            link: 'about.html'
        },
        {
            name: 'Contact',
            link: '#',
            children: [{
                    name: 'Twitter',
                    link: 'https://twitter.com/w_zce'
                },
                {
                    name: 'About',
                    link: 'https://weibo.com/zceme'
                },
                {
                    name: 'divider'
                },
                {
                    name: 'About',
                    link: 'https://github.com/zce'
                }
            ]
        }
    ],
    pkg: require('./package.json'),
    date: new Date()
}

//page任务,指定目录,保留路径,swig模板引擎,指定临时目录,项目变化推送浏览器
const page = () => {
    return src('src/*.html', { base: 'src' })
        .pipe(plugins.swig({ data, defaults: { cache: false } }))
        .pipe(dest('temp'))
        .pipe(bs.reload({ stream: true }))
}

//css任务,指定目录,保留路径,编译sass,指定临时目录,项目变化推送浏览器
const style = () => {
    return src('src/assets/styles/*.scss', { base: "src" })
        .pipe(plugins.sass())
        .pipe(dest('temp'))
        .pipe(bs.reload({ stream: true }))
};

//script任务 并支持ES6+语法 指定目录,保留路径,编译ES6语法,指定临时目录,项目变化推送浏览器
const script = () => {
    return src('src/assets/scripts/*.js', { base: 'src' })
        .pipe(babel({ presets: ['@babel/preset-env'] }))
        .pipe(dest('temp'))
        .pipe(bs.reload({ stream: true }))
}

//图片任务 压缩图片大小 输出到指定目录
const image = () => {
    return src('src/assets/images', { base: "images" })
        .pipe(plugins.imagemin())
        .pipe(dest('dist'))
};

//字体任务 压缩字体大小 输出到指定目录
const font = () => {
    return src('src/assets/fonts/**', { base: "src" })
        .pipe(plugins.imagemin())
        .pipe(dest('dist'))
};

//注释中间引用的文件任务
const useref = () => {
    return src('temp/*.html', { base: "temp" })

    .pipe(plugins.useref({ searchPath: ['temp', '.'] }))

    //压缩js
    .pipe(plugins.if(/\.js$/, plugins.uglify()))

    //压缩css
    .pipe(plugins.if(/\.css$/, plugins.cleanCss()))

    //压缩HTML
    .pipe(plugins.if(/\.html$/, plugins.htmlmin({ collapseWhitespace: true, minifyCSS: true, minifyJS: true })))

    .pipe(dest('dist'))
};

//其他文件任务 输出到指定目录
const extra = () => {
    return src("public/**", { base: "public" })
        .pipe(dest('dist'))
}

//clean删除文件任务
const clean = () => {
    return del(['dist', 'temp'])
}

//开发服务器任务
const serve = () => {
    //监视HTML变化
    watch("src/*.html", page);
    //监视scss变化
    watch("src/assets/styles/*.scss", style);
    //监视js变化
    watch("src/assets/scripts/*.js", script);
    //监视字体,图片,公共目录
    watch(["src/assets/fonts/**", "src/assets/images/**", "public/**"], bs.reload);

    bs.init({
        notify: false,
        server: {
            baseDir: ["temp", 'src', 'public'],
            routes: {
                "/node_modules": "node_modules"
            }
        },
    })
}

//compile任务 也就是组合任务 将页面,样式组合到一起
const compile = parallel(page, style, script);

//开发任务,编译任务 + 开发服务器 
const develop = series(compile, serve);

//build串行任务,clean =>  parallel(series(compile, useref) => font, image, extra
const build = series(clean, parallel(series(compile, useref), font, image, extra))

//导出任务
module.exports = {
    compile,
    build,
    develop,
    clean
}